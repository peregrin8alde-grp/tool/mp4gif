#!/bin/bash
# cmake / opencv / ffmpeg / gstreamer のインストール
# ホストと GID / UID を合わせるためのグループ／ユーザの作成
# 参考 : https://github.com/devcontainers/templates/blob/main/src/cpp/.devcontainer/reinstall-cmake.sh
set -e

CMAKE_VERSION=3.27.2
CMAKE_TGZ_NAME="cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz"

GROUPNAME=devgroup
USERNAME=devuser

cleanup() {
    EXIT_CODE=$?
    set +e
    if [[ -n "${TMP_DIR}" ]]; then
        rm -rf "${TMP_DIR}"
    fi
    exit ${EXIT_CODE}
}
trap cleanup EXIT

TMP_DIR=$(mktemp -d -t setup-XXXXXXXXXX)

cd "${TMP_DIR}"

curl -sSL \
  -O \
  "https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/${CMAKE_TGZ_NAME}"

mkdir -p /opt/cmake
tar zxvf "${TMP_DIR}/${CMAKE_TGZ_NAME}" -C /opt/cmake --strip-components 1

ln -s /opt/cmake/bin/cmake /usr/local/bin/cmake
ln -s /opt/cmake/bin/ctest /usr/local/bin/ctest

apt-get update \
&& apt-get install -y libopencv-dev \
&& apt-get install -y ffmpeg \
&& apt-get install -y libgstreamer1.0-dev \
  libgstreamer-plugins-base1.0-dev \
  libgstreamer-plugins-bad1.0-dev \
  gstreamer1.0-plugins-base \
  gstreamer1.0-plugins-good \
  gstreamer1.0-plugins-bad \
  gstreamer1.0-plugins-ugly \
  gstreamer1.0-libav \
  gstreamer1.0-tools \
  gstreamer1.0-x \
  gstreamer1.0-alsa \
  gstreamer1.0-gl \
  gstreamer1.0-gtk3 \
  gstreamer1.0-qt5 \
  gstreamer1.0-pulseaudio \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

groupadd -g 1000 "${GROUPNAME}"
useradd -m -u 1000 -g "${GROUPNAME}" -s /bin/bash "${USERNAME}"
