#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/videoio.hpp>
#include <webp/encode.h>

using namespace std;

int main(int argc, char** argv) {
  if (argc == 1) {
    cout << cv::getBuildInformation() << endl;
  }

  if (argc != 3) {
    printf("usage: Mp4Gif <Mp4_Path> <Gif_Path>\n");
    return -1;
  }

  string inFilePath = argv[1];
  string outFilePath = argv[2];

  // https://docs.opencv.org/4.8.0/dd/de7/group__videoio.html
  cv::VideoCapture videoCapture(inFilePath, cv::CAP_FFMPEG);
  if (!videoCapture.isOpened()) {
    cout << "can not open input video" << endl;

    return -1;
  }

  int fourcc = videoCapture.get(cv::CAP_PROP_FOURCC);
  cout << fourcc << endl;
  char fourcc_char[] = {(char)(fourcc & 0XFF), (char)((fourcc & 0XFF00) >> 8),
                        (char)((fourcc & 0XFF0000) >> 16),
                        (char)((fourcc & 0XFF000000) >> 24), 0};
  std::string fourcc_str = fourcc_char;
  cout << fourcc_str << endl;

  // http://ffmpeg.org/doxygen/trunk/isom_8c-source.html
  // gif 指定だとうまくいかない？
  // int codec = cv::VideoWriter::fourcc('g', 'i', 'f', ' ');
  int codec = cv::VideoWriter::fourcc('w', 'e', 'b', 'p');
  // int fps = videoCapture.get(cv::CAP_PROP_FPS);
  int fps = 10;
  int width = videoCapture.get(cv::CAP_PROP_FRAME_WIDTH);
  int height = videoCapture.get(cv::CAP_PROP_FRAME_HEIGHT);
  cv::Size size = cv::Size(width, height);
  cv::VideoWriter videoWriter(outFilePath, cv::CAP_FFMPEG, codec, fps, size);
  if (!videoWriter.isOpened()) {
    cout << "can not open output gif" << endl;

    return -1;
  }

  cv::Mat src;

  cout << "Start writing" << endl;
  for (;;)  // Show the image captured in the window and repeat
  {
    videoCapture >> src;
    if (src.empty()) {
      break;
    }

    videoWriter << src;
  }
  cout << "Finished writing" << endl;

  return 0;
}
