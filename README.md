# video2animeimg

自分の作成したツールや資料で画面の操作説明用に動画作成する際に、 mp4 で録画した短い動画を gif に変換して画像として asciidoc などの組み込むための変換ツール。

操作画面のキャプチャは windows の標準 snipping tool を利用する想定。
`Win + Shift + s` か Windows の設定で printscr に割り当てて起動、 `Pictures\Screenshots` に保存されるので適宜名前変更

OpenCV を利用？

https://opencv.org/releases/

OpenCV だと gif 出力できない？

ビデオ操作のバックエンドとして ffmpeg 

ffmpeg 自体、コマンドラインで mp4 => gif 変換可能であることに気づいたが、勉強としてツール作成継続

```
ffmpeg -i data/tmp.mp4 -r 10 -loop 0 data/aaa.gif

ffmpeg -i data/tmp.mp4 -r 10 -loop 0 data/aaa.webp
```

gif より webp の方が開発しやすそうなため、プロジェクト名と合わせて方針変更予定
mp4gif => video2animeimg

https://developers.google.com/speed/webp/docs/api?hl=ja

